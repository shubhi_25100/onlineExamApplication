import React, {  useState } from 'react';
import {Form,Input,Button,notification} from 'antd';
import { MailOutlined } from '@ant-design/icons';
import {auth} from '../firebase';
import { sendPasswordResetEmail } from 'firebase/auth';
import useNavigation from '../../hooks/use-Navigation';
import './forgot.css';
function ForgotPassword()
{
const [email,SetEmail] = useState(""); 
const {navigate} = useNavigation();  //to redirect to login page has used the hook->useNAvigation()
const handleSubmit= async(e)=>{
  e.preventDefault();
   await sendPasswordResetEmail(auth,email) 
  .then(()=>{ 
    notification.success({
      message:"Password Reset Email",
      description:`Password reset email has been sent to ${email}. Kindly,check your Inbox!`
      
    });
    navigate('/Admin');
})
  .catch((error)=>{
    notification.error({ 
     message: "Password reset Failed!",
     description: error.message, 
  });
});
}

return (  
<div className='forgot-page'> 
<div className='forgotpassword-wrapper'>
  
  <h3> Reset Password</h3>
  
    <Form name="forgotpwd" className='forgot-password'>
    <Form.Item 
      name="email"
      rules={[{required:true ,message: 'Please provide email' }]}
    > 
      <Input 
       prefix={<MailOutlined />}
       placeholder="email"
       value={email}
       onChange={(e)=>SetEmail(e.target.value)}
     />  
    </Form.Item> 

    <Form.Item>
     <Button type="primary" htmlType='submit' className="reset-password-button" onClick={handleSubmit}>
       Reset password
      </Button>  
     
    </Form.Item> 
    
    </Form> 
</div> 
</div>
);
}
export default ForgotPassword; 