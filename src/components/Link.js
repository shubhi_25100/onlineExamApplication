import classNames from 'classnames';
import useNavigation from '../hooks/use-Navigation';

function Link({ to, children }) {
  const { navigate, currentPath } = useNavigation();

  // Determine if the current link is active
  //const isActive = currentPath === to;

 const classes = classNames(
     'text-blue-500',// Default link color
     'mb-3',  // Margin bottom
    // className,  // Custom class name passed as a prop
    // { [activeClassName]: isActive } 
  );
   const handleClick = (event) => {
    if (event.metaKey || event.ctrlKey) {
      return;
    }
    event.preventDefault();

    navigate(to);
  };

  return (
    <a className={classes} href={to} onClick={handleClick}>
      {children}
    </a>
  );
}

export default Link;
