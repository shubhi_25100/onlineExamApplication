import React from 'react';
import './Home.css';
import Navbar from '../Navbar';

function Home() {
  return (
 <>
 <Navbar/>
    <div className='home-background'>
      
        Welcome to Online Examination App
    </div>
    </>
  );
}

export default Home;
