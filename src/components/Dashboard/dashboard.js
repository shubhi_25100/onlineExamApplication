import React, { useEffect } from 'react';
import { UploadOutlined, UserOutlined, VideoCameraOutlined} from '@ant-design/icons';
import { Layout, Menu, Avatar, Dropdown,theme } from 'antd';
import { BrowserRouter as Router, Routes, Route, Link, useLocation, useNavigate } from 'react-router-dom';
import './dashboard.css';
import DashboardContent from './DashboardContent';
import QuestionContent from './QuesContent';
import CourseContent from './CourseContent';

const { Header, Content, Footer, Sider } = Layout;

const menuItems = [
  {
    key: '1',
    icon: React.createElement(UserOutlined),
    label: <Link to="/dash-board">Dashboard</Link>,
  },
  {
    key: '2',
    icon: React.createElement(VideoCameraOutlined),
    label: <Link to="/questions">Questions</Link>,
  },
  {
    key: '3',
    icon: React.createElement(UploadOutlined),
    label: <Link to="/courses">Courses</Link>,
  },
];

const Dashboard = () => {
  const {
    token: { colorBgContainer, borderRadiusLG },
  } = theme.useToken();

  const navigate = useNavigate();
  const location = useLocation();

  const handleLogout = () => {
    console.log("Logout button clicked");
    navigate('/Admin');        //navigate to admin or login page
  };

  useEffect(() => {
    const lastPath = localStorage.getItem('lastPath');
    if (lastPath && location.pathname === '/') {
      navigate(lastPath, { replace: true });
    }
  }, [location.pathname, navigate]);

  useEffect(() => {
    if (location.pathname !== '/') {
      localStorage.setItem('lastPath', location.pathname);
    }
  }, [location.pathname]);

  const dropdownItems = [
    {
      key: '1',
      label: (
        <span onClick={handleLogout}>Logout</span>
      ),
    },
  ];

  return (
    <Layout className="dashboard-container">
      <Sider
        breakpoint="lg"
        collapsedWidth="0"
        onBreakpoint={(broken) => {
          console.log(broken);
        }}
        onCollapse={(collapsed, type) => {
          console.log(collapsed, type);
        }}
      >
        <div className="demo-logo-vertical" />
        <Menu className="custom-menu" theme="dark" mode="inline" defaultSelectedKeys={['1']} items={menuItems} />
      </Sider>
      <Layout>
        <Header className="dashboard-header">
          <div className="header-content-right">
            <Dropdown
              overlay={<Menu items={dropdownItems} />}
              placement="bottomRight"
              arrow
            >
              <Avatar icon={<UserOutlined className="custom-avatar-icon" />} />
            </Dropdown>
          </div>
        </Header>
        <Content className="dashboard-content">
          <div
            className="dashboard-inner-content"
            style={{
              background: colorBgContainer,
              borderRadius: borderRadiusLG,
            }}
          >
            <Routes>
              <Route path="/dash-board" element={<DashboardContent />} />
              <Route path="/questions" element={<QuestionContent />} />
              <Route path="/courses" element={<CourseContent />} />
              <Route path="/Admin-dashboard/" element={<DashboardContent />} />
            </Routes>
          </div>
        </Content>
        <Footer className="dashboard-footer">
          Ant Design ©{new Date().getFullYear()} Created by Ant UED
        </Footer>
      </Layout>
    </Layout>
  );
};

const App = () => (
  <Router>
    <Dashboard />
  </Router>
);

export default App;
