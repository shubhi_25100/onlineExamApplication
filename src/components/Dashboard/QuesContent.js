import React, { useEffect, useState } from 'react';
import { EditOutlined, EllipsisOutlined, SettingOutlined, QuestionCircleOutlined } from '@ant-design/icons';
import { Avatar, Button, Card, Modal, Form, Input, notification, Select } from 'antd';
import { doc, setDoc, collection, getDocs } from 'firebase/firestore';
import { db } from '../firebase';
import CourseContent from './CourseContent';

const { Meta } = Card;
const { Option } = Select;

const QuestionContent = () => {
  const [openForm, setOpenForm] = useState(false);
  const [loading, setLoading] = useState(false);
  const [openView, setOpenView] = useState(false);
  const [courseName, setCourseName] = useState("");
  const [question, setQuestion] = useState("");
  const [option1, setOption1] = useState("");
  const [option2, setOption2] = useState("");
  const [option3, setOption3] = useState("");
  const [option4, setOption4] = useState("");
  const [marks, setMarks] = useState("");
  const [courses, setCourses] = useState([]);

  // Fetch course names from Firestore
  const fetchCourses = async () => {
    try {
      const quesListDb = await getDocs(collection(db, "CourseTable"));
      const courseLists = [];
      quesListDb.forEach((doc) => {
      courseLists.push({ id: doc.id, ...doc.data() });
      });
      setCourses(courseLists);
      console.log("Courses fetched: ", courseLists); // Logging fetched data
    } catch (error) {
      console.error('Error fetching courses:', error.message);
      notification.error({
        message: 'Fetch Courses Failed',
        description: error.message,
      });
    }
  };

  useEffect(() => {
    fetchCourses();
  }, []);

  const handleUser = () => {
    setOpenForm(true);
  };

  const handleCancel = () => {
    setOpenForm(false);
  };

  const onFinish = async () => {
    if (!courseName) {
      notification.error({
        message: "Add Course!",
        description: "Course Name should not be empty!",
      });
      return;
    }
    setLoading(true);
    try {
      await setDoc(doc(db, 'QuestionTable', question), {
        Course: courseName,
        Question: question,
        Marks: marks,
        Option1: option1,
        Option2: option2,
        Option3: option3,
        Option4: option4,
      });
      notification.success({
        message: 'Question Added Successfully',
        description: 'The question has been added successfully!',
      });
      setOpenForm(false); // closing the modal/form after submission
      setCourseName("");
      setQuestion("");
      setOption1("");
      setOption2("");
      setOption3("");
      setOption4("");
      setMarks("");
    } catch (error) {
      console.error('Error adding question:', error.message);
      notification.error({
        message: 'Add Question Failed',
        description: error.message,
      });
    } finally {
      setLoading(false);
    }
  };

  return (
    <>
      <div className='flex-container'>
        <Card
          className="card-container"
          cover={<div className="card-cover"></div>}
          actions={[<SettingOutlined key="setting" />, <EditOutlined key="edit" />, <EllipsisOutlined key="ellipsis" />]}
        >
          <Button className="add-question-button" onClick={handleUser}>
            <Meta
              className="meta-container"
              avatar={<Avatar className="meta-avatar" src="https://api.dicebear.com/7.x/miniavs/svg?seed=8" />}
              title={<span className="meta-title">Add Question</span>}
            />
          </Button>
        </Card>

        <Modal title="Add Question" visible={openForm} onCancel={handleCancel} footer={null}>
          <Form
            name="add-question"
            initialValues={{ remember: true }}
            onFinish={onFinish}
            autoComplete="off"
          >
          <Form.Item
              name="Course"
              rules={[{ required: true, message: 'Please select a course!' }]}
            >  
              <Select
                placeholder="Select a course"
                value={courseName} 
                onChange={(value) => setCourseName(value)}
              >
                {courses.map((course) => ( 
                  <Option key={course.id} value={course.Course}>
                    {course.Course} 
                  </Option>
                ))}
              </Select> 
            </Form.Item>
            <Form.Item
              name="Question"
              rules={[{ required: true, message: 'Please input your question!' }]}
            >
              <Input
                placeholder="Question"
                value={question}
                onChange={(e) => setQuestion(e.target.value)}
              />
            </Form.Item>
            <Form.Item
              name="Option1"
              rules={[{ required: true, message: 'Please input option 1!' }]}
            >
              <Input 
                placeholder="Option 1"
                value={option1}
                onChange={(e) => setOption1(e.target.value)}
              />
            </Form.Item>
            <Form.Item
              name="Option2"
              rules={[{ required: true, message: 'Please input option 2!' }]}
            >
              <Input
                placeholder="Option 2"
                value={option2} 
                onChange={(e) => setOption2(e.target.value)}
              />
            </Form.Item>
            <Form.Item
              name="Option3"
              rules={[{ required: true, message: 'Please input option 3!' }]}
            >
              <Input
                placeholder="Option 3"
                value={option3}
                onChange={(e) => setOption3(e.target.value)}
              />
            </Form.Item>
            <Form.Item
              name="Option4"
              rules={[{ required: true, message: 'Please input option 4!' }]}
            >
              <Input
                placeholder="Option 4"
                value={option4}
                onChange={(e) => setOption4(e.target.value)}
              />
            </Form.Item>
            <Form.Item
              name="Correct Answer"
              rules={[{ required: true, message: 'Input Correct Answer!' }]}
            >
              <Input
                placeholder="Input correct answer"
                value={marks}
                onChange={(e) => setMarks(e.target.value)}
              />
            </Form.Item>
            <Form.Item>
              <Button type="primary" htmlType="submit" loading={loading}>
                Add
              </Button>
            </Form.Item>
          </Form>
        </Modal>

        <Card
          className="card-container"
          cover={<div className="card-cover"></div>}
          actions={[]}
        >
          <label className="view-questions-label">
            <Meta
              className="meta-container"
              title={<span className="meta-title"><QuestionCircleOutlined /> View Questions</span>}
            />
          </label>
          <Button>
            View
          </Button>
        </Card>
        <Modal title="QuestionList" visible={openView} onhandle={handleCancel}>
          <Form class="View-Questions">
            <Form.Item 
            name="View-questionList"
            rules={[{ required: true , message: 'select a respective course for queslist'}]}
            >
            <Select
                placeholder="Select a course"
                value={courseName} 
                onClick={(value) => setCourseName(value)}
              >
            {courses.map((course)=>{
              <Option key={course.id} value={course.Course}>
                {course.Course}
              </Option>    
              })}
            </Select>
          </Form.Item>
          </Form>
        </Modal> 
      </div>
    </>
  );
};

export default QuestionContent;
