import React, { useState} from 'react';
import { EditOutlined, EllipsisOutlined, SettingOutlined, QuestionCircleOutlined } from '@ant-design/icons';
import { Avatar, Button, Card, Modal, Form, Input, notification, Table } from 'antd';
import { doc, setDoc, collection, getDocs } from 'firebase/firestore';
import { db } from '../firebase';

const { Meta } = Card;

const CourseContent = () => {
  const [openForm, setOpenForm] = useState(false);
  const [openView, setOpenView] = useState(false);
  const [loading, setLoading] = useState(false);
  const [courseName, setCourseName] = useState("");
  const [question, setQuestion] = useState("");
  const [marks, setMarks] = useState("");
  const [courses, setCourses] = useState([]);

  const handleUser = () => {    
    setOpenForm(true); 
  };
   
  const handleCancel = () => { 
    setOpenForm(false);
  };

  const handleView = async () => {
    setOpenView(true);
    try { 
      const courseListDisplay = await getDocs(collection(db, "CourseTable"));
      const coursesList = [];
      courseListDisplay.forEach((doc) => {  
      coursesList.push({ key: doc.id, ...doc.data() });
      });
      setCourses(coursesList);
    } catch (error) {  
      console.error('Error fetching courses:', error.message);
      notification.error({ 
        message: 'Fetch Courses Failed',
        description: error.message,
      });
    }
  };

  const handleViewCancel = () => {
    setOpenView(false);
  };

  const onFinish = async () => {
    if (!courseName) { 
      notification.error({
        message: "Error!",
        description: "Course Name should not be empty!",
      });
      return;
    }

    setLoading(true);
    try {
      await setDoc(doc(db, 'CourseTable', courseName), {
        Course: courseName,
        Question: question,
        Marks: marks,
      });
      notification.success({
        message: 'Course Added Successfully',
        description: 'The Course has been added successfully!',
      });
      setOpenForm(false); // Closing the modal/form after submission
      // Clear the form fields 
      setCourseName("");
      setQuestion("");
      setMarks("");
    } catch (error) {
      console.error('Error adding question:', error.message);
      notification.error({
        message: 'Add Question Failed',
        description: error.message,
      });
    } finally {
      setLoading(false);
    }
  };

  const columns = [
    {
      title: 'Course',
      dataIndex: 'Course',
      key: 'course',
    },
    {
      title: 'Question',
      dataIndex: 'Question',
      key: 'question',
    },
    {
      title: 'Marks',
      dataIndex: 'Marks',
      key: 'marks',
    },
  ];

  return (
    <>
      <div className='flex-container'> 
        <Card
          className="card-container"
          cover={<div className="card-cover"></div>}
          actions={[<SettingOutlined key="setting" />, <EditOutlined key="edit" />, <EllipsisOutlined key="ellipsis" />]}
        >
          <Button className="add-question-button" onClick={handleUser}>
            <Meta
              className="meta-container"
              avatar={<Avatar className="meta-avatar" src="https://api.dicebear.com/7.x/miniavs/svg?seed=8" />}
              title={<span className="meta-title">Add Courses</span>}
            />
          </Button>
        </Card>

        <Modal title="Add Course" visible={openForm} onCancel={handleCancel} footer={null}>
          <Form
            name="add-question"
            initialValues={{ remember: true }}
            onFinish={onFinish}
            autoComplete="off"
          >
            <Form.Item
              name="Course"
              rules={[
                { required: true, message: 'Please Add a course!' },
                {
                  pattern: /^[a-zA-Z0-9.#,+-]+$/,
                  message: 'Please enter a correct course name!',
                }
              ]}
            >
              <Input
                placeholder="Add Course"
                value={courseName}
                onChange={(e) => setCourseName(e.target.value)}
              />
            </Form.Item>
            <Form.Item
              name="Question"
              rules={[{ required: true, message: 'Please input your question!' }]}
            >
              <Input
                placeholder="Question"
                value={question}
                onChange={(e) => setQuestion(e.target.value)}
              />
            </Form.Item> 
            <Form.Item
              name="Marks"
              rules={[{ required: true, message: 'Please input marks!' }]}
            >
              <Input
                placeholder="Marks"
                value={marks} 
                onChange={(e) => setMarks(e.target.value)}
              />
            </Form.Item> 
            <Form.Item> 
              <Button type="primary" htmlType="submit" loading={loading}>
                Add
              </Button> 
            </Form.Item> 
          </Form> 
        </Modal>

        <Card
          className="card-container"
          cover={<div className="card-cover"></div>}
          actions={[]} 
        >
          <label className="view-questions-label">
            <Meta
              className="meta-container"
              title={<span className="meta-title"><QuestionCircleOutlined /> View courses</span>}
            />
          </label> 
          <Button onClick={handleView}>View</Button> 

        </Card> 

        <Modal title="Course List" visible={openView} onCancel={handleViewCancel} footer={null}>
          <Table columns={columns} dataSource={courses} pagination={false}  />
        </Modal> 
      </div> 
    </>
  );
};

export default CourseContent;
