import React, { useState } from 'react';
import { EditOutlined, EllipsisOutlined, SettingOutlined,UserOutlined,MailOutlined,LockOutlined, SnippetsOutlined,QuestionCircleOutlined,UserAddOutlined } from '@ant-design/icons';
import { Avatar, Button, Card, Modal, Form, Input ,notification,Radio} from 'antd';
import './DashboardContent.css'; // Import the CSS file
import { createUserWithEmailAndPassword } from "firebase/auth";
import { doc, getDoc, setDoc } from 'firebase/firestore';
import { auth,db } from '../firebase'; 
import Link from '../Link';
const { Meta } = Card;

const DashboardContent = () => {
  const [openForm, setOpenForm] = useState(false);
  const [loading,setLoading] =useState(false);
  const [role,setRole]  =useState();
  const [Fname, setFname] = useState("");
  const [Lname, setLname] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState(""); 


  const handleUser = () => {
    setOpenForm(true);
  };
 
  const handleOk = () => {
    setOpenForm(false);
  };

  const handleCancel = () => {
    setOpenForm(false);
  };

  const onFinish = async (values) => {
    setLoading(true);
    try {
      // Create user with email and password
      const userCredential = await createUserWithEmailAndPassword(auth, email, password);
      const user = userCredential.user;
      
      // Store user details in Firestore
      if(user){
      await setDoc(doc(db,'Users', user.uid), {
        firstName: Fname,
        lastName: Lname,
        email: email,
        role:role,
      });
    }
      console.log("User Registered Successfully!", user);
      notification.success({
        message: 'Registration Successful',
        description: 'You have successfully registered!',
      });
    } catch (error) {
      console.error('Registration error:', error.message);
      notification.error({
        message: 'Registration Failed',
        description: error.message,
      });
    }
    finally { 
      setLoading(false);
    }
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <> 
    <div className='flex-container'>
      <Card
        className="card-container"
        cover={
          <div className="card-cover">
          
          </div> 
        }
        actions={[
          <SettingOutlined key="setting" />,
          <EditOutlined key="edit" />,
          <EllipsisOutlined key="ellipsis" />,
        ]}
      >
        <Button className="add-user-button" onClick={handleUser}>
          <Meta
            className="meta-container"
            avatar={<Avatar className="meta-avatar" src="https://api.dicebear.com/7.x/miniavs/svg?seed=8" />}
            title={<span className="meta-title">Add User</span>}
          /> 
        </Button> 
      </Card>

      <Modal title="Add User" visible={openForm} onOk={handleOk} onCancel={handleCancel} footer={null}>
        <Form
          name="basic"
          initialValues={{ remember: true }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        > 
           <Form.Item
          name="Fname" 
          rules={[{ required: true, message: 'Please input your First Name' }]}
        >
          <Input 
            prefix={<UserOutlined />} 
            placeholder="First Name" 
            value={Fname}
            onChange={(e) => setFname(e.target.value)}
            autoComplete="given-name"
          />
        </Form.Item>
        <Form.Item
          name="Lname"
          rules={[{ required: true, message: 'Please input your Last Name!' }]}
        >
          <Input 
            prefix={<UserOutlined />} 
            placeholder="Last Name" 
            value={Lname}
            onChange={(e) => setLname(e.target.value)} 
            autoComplete="family-name"
          /> 
        </Form.Item>
        <Form.Item 
          name="email"
          rules={[
            { required: true, message: 'Please input your Email!' },
            { type: 'email', message: 'The input is not valid E-mail!' }
          ]}
        >
          <Input 
            prefix={<MailOutlined />} 
            placeholder="Email" 
            value={email}
            onChange={(e) => setEmail(e.target.value)} 
            autoComplete="email"
          /> 
        </Form.Item>
        <Form.Item 
          name="password" 
          rules={[{ required: true, message: 'Please input your Password!' }]}
          hasFeedback
        >
          <Input.Password 
            prefix={<LockOutlined />} 
            placeholder="Password" 
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            autoComplete="new-password"
          />
        </Form.Item>
        <Form.Item
            name="role"
            rules={[{ required: true, message: 'Please select a role!' }]}
          >
            <Radio.Group onChange={(e) => setRole(e.target.value)} value={role}>
              <Radio value="user">User</Radio>
              <Radio value="admin">Admin</Radio> 
            </Radio.Group> 
          </Form.Item> 

        <Form.Item>
          <Button type="primary" htmlType="submit" className="register-form-button" loading={loading}>
            Add
          </Button> 
        </Form.Item> 
      </Form>
      </Modal>
 
      <Card
        className="card-container"
        cover={
          <div className="card-cover">
 
          </div> 
        }
        actions={[
          
        ]}
      >
        <label className="Total Questions">
          <Meta
            className="meta-container"
            title={<span className="meta-title">
              <QuestionCircleOutlined />
              Total Questions
              </span>}
          />  
        </label> 
      </Card>

      <Card
        className="card-container"
        cover={
          <div className="card-cover">
            
          </div> 
        }
        actions={[
          
        ]}
      >
        <label className="Total Courses">
          <Meta
            className="meta-container"
           
            title={<span className="meta-title">
              <SnippetsOutlined />
              Total Courses</span>} 
          />  
        </label> 
      </Card>
    
     </div>
    </>

  );
};

export default DashboardContent;
