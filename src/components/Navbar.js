import Link from './Link';
import './Navbar.css';
import { DatabaseFilled, HomeFilled , UserAddOutlined  } from "@ant-design/icons";

function Navbar(){
    const Links=[
     {label:'Home' ,path:'/'},
     {label:'Admin',path:'/Admin'}
    ];
    const renderedLinks = Links.map((link)=>{
        
        return (  
        <li key={link.label} className='navbar-item'>
        <Link
            to={link.path}
            className="navbar-link"
            activeClassName="active-link"
        >
            {link.label === 'Home' && <HomeFilled style={{ marginRight: '0.5rem' }} />}
            {link.label === 'Admin' && <UserAddOutlined style={{ marginLeft: '0rem', marginRight: '0.5rem' }} />}
            {link.label}
        </Link>
    </li>
        );
    })

    return( 
        <div className="navbar">
          <DatabaseFilled style={{ marginRight: '2rem' }} /> {/* Adjust margin as needed */}
        <ul className="navbar-links" >
          {renderedLinks}
        </ul>
      </div> 
    );
}

export default Navbar;