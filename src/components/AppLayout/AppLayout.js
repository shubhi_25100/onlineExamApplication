// src/components/AppLayout.jsx
import React from 'react';
import Navbar from './Navbar'; // Adjust the import path if needed
import './AppLayout.css'; // Import the layout CSS

const AppLayout = ({ children }) => {
  return (
    <div className="app-layout">
      <Navbar />
      <div className="content">
        {children}
      </div>
    </div>
  );
};

export default AppLayout;
