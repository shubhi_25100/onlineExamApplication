// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import {getAuth} from 'firebase/auth';
import {getFirestore } from "firebase/firestore";

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyB9EHZC1UCqijqrOxmQ3_nVD9U1smafhVk",
  authDomain: "online-examination-app-c0fe5.firebaseapp.com",
  databaseURL: "https://online-examination-app-c0fe5-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "online-examination-app-c0fe5",
  storageBucket: "online-examination-app-c0fe5.appspot.com",
  messagingSenderId: "488552952616",
  appId: "1:488552952616:web:c19765b458bb064fd0961d" 
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const auth=getAuth();
export const db=getFirestore();

export default app;
