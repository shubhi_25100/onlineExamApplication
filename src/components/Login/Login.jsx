import { MailOutlined } from '@ant-design/icons';
import {Form , Input , Button , notification} from 'antd';
import { useState } from 'react';
import { signInWithEmailAndPassword } from 'firebase/auth';
import { auth,db } from '../firebase';
function Login(){
const [email,setEmail]=useState("");
const [password,setPassword] =useState("");

const handleLogin= async(e)=>{
 e.prevent.Default();
 try{
    const userCredential= await signInWithEmailAndPassword(auth,email,password);
      const user=userCredential.user;
      console.log("User loggedIn successfully!!",user);
      notification.success({
        message: 'UserLogin Successfully!!',
        description: 'You have successfully loggedin!',
      });

 }
 catch(error){
    console.error('LoggedIn error:', error.message);
    notification.error({
      message: 'Login Failed!!',
      description: error.message,
    });
 }
};

return (
<div classname="login-wrapper">
    <Form className="login-form"
      onSubmit={handleLogin}
    >
      <h2>Login</h2>
      <Form.Item 
        name="Login"
        value={email}
        className="login-form"
        rules={[
            {required:true , message:"Please provide Email"}
        ]}
       > 
        <Input
         prefix={<MailOutlined/>}
         placeholder='Email'
         autoComplete="email"
         onChange={(e)=>setEmail(e.target.value)}
        />
      </Form.Item> 

      <Form.Item 
       name="password"
       value={password}
       rules={[ {required:true ,message: 'Please provide Password'} ]}
       autoComplete="password"
       onChange={(e)=>setPassword(e.target.value)}
       >
      </Form.Item>

      <Form.Item>
        <Button type="primary" htmlType='submit' className='login-form-button'>
         Login 
        </Button>
      </Form.Item>
    </Form>
</div>
);
};

export default Login;