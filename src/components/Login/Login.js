import React, { useEffect, useState } from 'react';
import { LockOutlined, MailOutlined } from '@ant-design/icons';
import { Form, Input, Button, notification } from 'antd';
import { signInWithEmailAndPassword } from 'firebase/auth';
import { auth,db } from '../firebase';
import './Login.css'; 
import Link from '../Link';
import useNavigation from '../../hooks/use-Navigation';
import Navbar from '../Navbar';
import { getDoc,doc } from 'firebase/firestore';

function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(false);              // Loading state for navigation
  const {navigate}  = useNavigation();
  const [rememberMe,setRememberMe] = useState(false);

  const handleLogin = async (values) => {  
    try {
      
      const userCredential = await signInWithEmailAndPassword(auth, values.email, values.password);
      const user = userCredential.user;
      const userdoc = await getDoc(doc(db,'Users',user.uid));
      const  userdata = userdoc.data();

      if(userdata.role === 'admin'){  
        notification.success({ 
          message: 'Login successfull',
          description:'you have successfully logged-in!',
        });
        if(rememberMe){
          localStorage.getItem('Remember me','true');
        }
        else{
          localStorage.removeItem('Remember me');
        }
        navigate('/Admin-Dashboard');
     } 
     else{
       throw new Error("You do not have access to the admin dashboard");
     }
    } catch (error) { 
      console.error('Login error:', error.message);
      if(error.code === 'auth/wrong-password'){
        error.message = 'invalid password for the entered email address';
      } else if(error.code === 'auth/user-not-found'){
        error.message='No user found with this email address'
      }
      else if(error.code === 'auth/invalid-email'){
        error.message = 'The email address is badly formatted';
      }
      else if(error.code === 'auth/invalid-credential'){
        error.message= 'The password is incorrect';
      }

      notification.error({ 
        message: 'Login Failed',
        description:error.message,
      });
      setLoading(false);           // Hide loading indicator on error
    } 
  };
  useEffect(() => {
    const rememberMeValue = localStorage.getItem('rememberMe');
    if (rememberMeValue === 'true') {
      setRememberMe(true);
    } 
    else{
      setRememberMe(false);
    }
  }, []);

  const handlechangeRememberme = (e) => {
    setRememberMe(e.target.checked); 
  };
  

  return (
    <>
    <Navbar />
    <div className="login-wrapper">
      <Form
        name="login"
        className="login-form"
        onFinish={handleLogin} // Correctly handle form submission with Ant Design
      > 
        <h2>Login</h2> 
        <Form.Item 
          name="email" 
          rules={[
            { required: true, message: "Please input your Email!" },
            { type: 'email', message: 'The input is not valid E-mail!' },
            {
              pattern: /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/,
              message: 'Please enter a valid email address!',
            }
          ]}
        > 
          <Input
            prefix={<MailOutlined style={{ color: '#999' }} />} // Adjust icon color to match placeholder
            placeholder='Email'
            autoComplete="email"
            value={email} 
            onChange={(e) => setEmail(e.target.value)} 
          /> 
        </Form.Item> 
        <Form.Item 
          name="password"
          rules={[
            { required: true, message: 'Please input your Password!'},
             { pattern: /^(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%#*?&]{8,}$/,
               message: 'Please enter valid password!',
            },

          ]}
        >
          <Input.Password
            prefix={<LockOutlined style={{ color: '#999' }} />} // Adjust icon color to match placeholder
            placeholder="Password"
            value={password} 
            autoComplete="current-password"
            onChange={(e) => setPassword(e.target.value)}
          /> 
        </Form.Item> 

        <Form.Item className="rem-forgot">
          <div className="rem-forgot">
            <label>
              <input type="checkbox" checked={rememberMe} onChange={handlechangeRememberme} />Remember me</label>
          </div> 
        </Form.Item> 

       <Form.Item className='forgot-passwrd'> 
            <Link to='/forgotpassword'>
              forgot password?
            </Link>
       </Form.Item>
       
        <Form.Item> 
          <Button type="primary" htmlType='submit' className='login-form-button'  loading={loading}>
            Login
          </Button> 
      </Form.Item> 
      </Form> 
    </div>
    </>
  );
}

export default Login;
