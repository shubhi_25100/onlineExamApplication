import React, { useState } from 'react';
import { Form, Input, Button , Radio , notification } from 'antd';
import { createUserWithEmailAndPassword } from "firebase/auth";
import { doc, getDoc, setDoc } from 'firebase/firestore';
import { auth,db } from '../firebase'; 
import { UserOutlined, MailOutlined, LockOutlined } from '@ant-design/icons';
import './register.css';    
import Link from '../Link';
import useNavigation from '../../hooks/use-Navigation';

function RegisterForm() {
  const [loading, setLoading] = useState(false);
  const [role,setRole]  =useState();
  const [Fname, setFname] = useState("");
  const [Lname, setLname] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState(""); 
  
  const onFinish = async (values) => {
    setLoading(true);
    try {
      // Create user with email and password
      const userCredential = await createUserWithEmailAndPassword(auth, email, password);
      const user = userCredential.user;
      
      // Store user details in Firestore
      if(user){
      await setDoc(doc(db,'Users', user.uid), {
        firstName: Fname,
        lastName: Lname,
        email: email,
        role:role,
      });
    }
      console.log("User Registered Successfully!", user);
      notification.success({
        message: 'Registration Successful',
        description: 'You have successfully registered!',
      });
    } catch (error) {
      console.error('Registration error:', error.message);
      notification.error({
        message: 'Registration Failed',
        description: error.message,
      });
    }
    finally { 
      setLoading(false);
    }
  };

  return (
    <div className='register-page'>
    <div className="register-form-wrapper">
      <Form
        name="register"
        className="register-form"
        onFinish={onFinish}
      >
        <h2>Sign Up</h2>
        <Form.Item
          name="Fname" 
          rules={[{ required: true, message: 'Please input your First Name' }]}
        >
          <Input 
            prefix={<UserOutlined />} 
            placeholder="First Name" 
            value={Fname}
            onChange={(e) => setFname(e.target.value)}
            autoComplete="given-name"
          />
        </Form.Item>
        <Form.Item
          name="Lname"
          rules={[{ required: true, message: 'Please input your Last Name!' }]}
        >
          <Input 
            prefix={<UserOutlined />} 
            placeholder="Last Name" 
            value={Lname}
            onChange={(e) => setLname(e.target.value)} 
            autoComplete="family-name"
          /> 
        </Form.Item>
        <Form.Item 
          name="email"
          rules={[
            { required: true, message: 'Please input your Email!' },
            { type: 'email', message: 'The input is not valid E-mail!' }
          ]}
        >
          <Input 
            prefix={<MailOutlined />} 
            placeholder="Email" 
            value={email}
            onChange={(e) => setEmail(e.target.value)} 
            autoComplete="email"
          /> 
        </Form.Item>
        <Form.Item 
          name="password" 
          rules={[{ required: true, message: 'Please input your Password!' }]}
          hasFeedback
        >
          <Input.Password 
            prefix={<LockOutlined />} 
            placeholder="Password" 
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            autoComplete="new-password"
          />
        </Form.Item>
        <Form.Item
            name="role"
            rules={[{ required: true, message: 'Please select a role!' }]}
          >
            <Radio.Group onChange={(e) => setRole(e.target.value)} value={role}>
              <Radio value="user">User</Radio>
              <Radio value="admin">Admin</Radio> 
            </Radio.Group> 
          </Form.Item> 

        <Form.Item>
          <Button type="primary" htmlType="submit" className="register-form-button" loading={loading}>
            Register
          </Button> 
        </Form.Item> 
        <div className='already-register'> 
          <h5>
            Already registered?
          <Link to='/Admin'>
             <u>
            Login Here
            </u>
            </Link> 
            </h5>
        </div>
      </Form>
    </div>
    </div>
  );
};

export default RegisterForm;
