import Register from '../components/Register/register.js';
import AppLayout from '../components/AppLayout/AppLayout.js';

function register(){
return(
    <AppLayout>
    <Register />
    </AppLayout>
    );
}
export default register;