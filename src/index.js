import React from 'react';
import {createRoot} from 'react-dom/client';
import './index.css';
import App from './App';
import { NavProvider } from './context/navigation';

const root = createRoot(document.getElementById('root'));
root.render(
    <NavProvider>
        <App />
    </NavProvider>
  

);


