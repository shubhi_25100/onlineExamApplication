import Route from './components/Route';
import HomePage from './Pages/HomePage';
import AdminPage from './Pages/AdminPage';
import Forgot from './components/Forgotpasword/forgot';
// import dash from './components/Dashboard/dashboard';
// import { Dashboard } from '@ant-design/icons';
import Dashboard from './components/Dashboard/dashboard';

function App() { 
  return (
    <div className="container mx-auto grid grid-cols-6 gap-4 mt-4" >
    <Route path="/"> 
          <HomePage /> 
      </Route> 
    <Route path="/Admin"> 
          <AdminPage /> 
      </Route> 
    <Route path="/forgotpassword"> 
          <Forgot />
      </Route> 
    <Route path="/Admin-Dashboard">
          <Dashboard />
      </Route> 
      </div> 
  );
}

export default App;
