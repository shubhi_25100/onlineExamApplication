import {createSlice} from '@reduxjs/toolkit';

const initialState=[{
    status:false,
     data:[]
}]
const  LoginformSlice = createSlice({
name : 'login',
initialState ,
reducers:{
    login :(state,action)=>{
          state.status = true;
          state.data= action.payload.data;
    },
    logout :(state)=>{ 
            state.status = false;
            state.data= null;

    }
}
});
export const {login,logout} = LoginformSlice.actions;
export default LoginformSlice.reducer;